package main

import (
	"fmt"
)

const (
	_     = iota
	year1 = (iota + 2018)
	year2 = (iota + 2018)
	year3 = (iota + 2018)
	year4 = (iota + 2018)
)

func main() {

	fmt.Println(year1)
	fmt.Println(year2)
	fmt.Println(year3)
	fmt.Println(year4)
}
