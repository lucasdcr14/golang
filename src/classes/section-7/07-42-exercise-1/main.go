package main

import (
	"fmt"
)

const (
	a = 42
)

func main() {

	fmt.Printf("%d\n", a)
	fmt.Printf("%b\n", a)
	fmt.Printf("%#X\n", a)
}
