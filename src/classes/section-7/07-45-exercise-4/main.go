package main

import (
	"fmt"
)

func main() {

	variable := 42

	fmt.Printf("%d\t%b\t%#X\n", variable, variable, variable)

	variable = variable << 1

	fmt.Printf("%d\t%b\t%#X\n", variable, variable, variable)
}
