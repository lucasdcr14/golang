package main

import (
	"fmt"
)

const a = 42
const b int = 42

func main() {

	fmt.Println(a)
	fmt.Println(b)
}
