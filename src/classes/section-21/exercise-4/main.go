package main

import (
	"fmt"
	"sync"
)

func main() {

	const number = 50
	total := 0

	var wg sync.WaitGroup
	wg.Add(number)

	var mu sync.Mutex

	for i := 0; i < number; i++ {
		go func() {
			mu.Lock()
			v := total
			v++
			total = v
			mu.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(total)
}
