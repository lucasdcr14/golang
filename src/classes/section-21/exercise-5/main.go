package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func main() {

	const number = 50
	var counter int64

	var wg sync.WaitGroup
	wg.Add(number)

	for i := 0; i < number; i++ {
		go func() {
			atomic.AddInt64(&counter, 1)
			runtime.Gosched()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
