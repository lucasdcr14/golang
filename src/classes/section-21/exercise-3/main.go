package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	const number = 50
	total := 0

	var wg sync.WaitGroup
	wg.Add(number)

	for i := 0; i < number; i++ {
		go func() {
			v := total
			runtime.Gosched()
			v++
			total = v
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(total)
}
