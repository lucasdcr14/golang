package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	fmt.Println("Number of CPUs ", runtime.NumCPU())
	fmt.Println("Number of goroutines ", runtime.NumGoroutine())

	const goroutines = 2
	var wg sync.WaitGroup
	wg.Add(goroutines)

	go func() {
		fmt.Println("First GoRoutine")
		wg.Done()
	}()

	go func() {
		fmt.Println("Second GoRoutine")
		wg.Done()
	}()

	wg.Wait()
	fmt.Println("Number of goroutines ", runtime.NumGoroutine())
}
