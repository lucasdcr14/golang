package main

import (
	"log"
	"os"
)

func main() {
	_, err := os.Open("no-file.txt")
	if err != nil {
		log.Panicln(err)
	}
}

/*
Panicln is equivalent to Println() followed by a call to panic()
*/

//Fatalln is equivalent to Println() followed by a call to os.Exit(1).
