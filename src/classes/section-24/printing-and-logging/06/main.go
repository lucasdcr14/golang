package main

import (
	"os"
)

func main() {
	_, err := os.Open("no-file.txt")
	if err != nil {
		panic(err)
	}
}

/*
... the Panic functions call panic after writing the loog message
*/

// http://godoc.org/builtin#panic
