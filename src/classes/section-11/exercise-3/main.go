package main

import "fmt"

func main() {
	slice := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}

	/* for i := 0; i < 5; i++ {
		fmt.Print(slice[i], "\t")
	}
	fmt.Println()

	for i := 5; i < 10; i++ {
		fmt.Print(slice[i], "\t")
	}
	fmt.Println()

	for i := 2; i < 7; i++ {
		fmt.Print(slice[i], "\t")
	}
	fmt.Println()

	for i := 1; i < 6; i++ {
		fmt.Print(slice[i], "\t")
	}
	fmt.Println() */

	fmt.Println(slice[:5])
	fmt.Println(slice[5:])
	fmt.Println(slice[2:7])
	fmt.Println(slice[1:6])

	fmt.Printf("%T", slice)
}
