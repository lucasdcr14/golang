package main

import (
	"fmt"
)

func main() {
	m := map[string][]string{
		"bond_james":      {"shaken, not stirred", "Martins", "Women"},
		"moneypenny_miss": {"James Bond", "Literature", "Computer Science"},
		"no_dr":           {"Being evil", "Ice cream", "Sunsets"},
	}

	for k, v := range m {
		fmt.Println(k)
		for i, v := range v {
			fmt.Println("\t", i, v)
		}
	}

	/* m := map[string][]string{
		"bond_james":      []string{"shaken, not stirred", "Martins", "Women"},
		"moneypenny_miss": []string{"James Bond", "Literature", "Computer Science"},
		"no_dr":           []string{"Being evil", "Ice cream", "Sunsets"},
	} */
}
