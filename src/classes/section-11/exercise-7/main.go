package main

import "fmt"

func main() {

	slice1 := []string{"James", "Bond", "Shaken, not stirred"}
	slice2 := []string{"Miss", "Moneypenny", "Helloooooo, James."}

	slice2d := [][]string{slice1, slice2}

	for i, v := range slice1 {
		fmt.Println(i, v)
	}

	for i, v := range slice2 {
		fmt.Println(i, v)
	}

	/* for i := 0; i < len(slice1); i++ {
		for j := 0; j < len(slice2); j++ {
			fmt.Println(slice2d[i][j])
		}
	} */

	for i, _ := range slice1 {
		fmt.Println("record: ", i)
		for j, v := range slice2 {
			fmt.Printf("\t index position: %v\t value:\t %v", j, v)
		}
	}
	fmt.Println(slice2d)
}
