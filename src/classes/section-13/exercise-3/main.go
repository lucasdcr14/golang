package main

import "fmt"

type vehicle struct {
	doors int
	color string
}

type truck struct {
	vehicle
	fourWheel bool
}

type sedan struct {
	vehicle
	luxury bool
}

func main() {

	v := vehicle{
		doors: 4,
		color: "Red",
	}

	/* t := truck{
		vehicle:   v,
		fourWheel: true,
	} */

	t := truck{
		vehicle: vehicle{
			doors: 2,
			color: "white",
		},
		fourWheel: true,
	}

	s := sedan{
		vehicle: v,
		luxury:  false,
	}

	fmt.Println(v)
	fmt.Println(t)
	fmt.Println(s)

	fmt.Println(t.doors)
	fmt.Println(t.color)
	fmt.Println(t.fourWheel)

	fmt.Println(s.doors)
	fmt.Println(s.color)
	fmt.Println(s.luxury)
}
