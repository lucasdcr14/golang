package main

import "fmt"

type person struct {
	firstName        string
	lastName         string
	favoriteIceCream []string
}

func main() {

	p1 := person{
		firstName: "Lucas",
		lastName:  "Ribeiro",
		favoriteIceCream: []string{
			"Goiaba",
			"Pequi",
			"Morango",
		},
	}

	p2 := person{
		firstName: "Nathalia",
		lastName:  "Oliveira",
		favoriteIceCream: []string{
			"Café",
			"Jurubeba",
			"Tomate",
		},
	}

	for i, v := range p1.favoriteIceCream {
		fmt.Println(i, v)
	}

	for i, v := range p2.favoriteIceCream {
		fmt.Println(i, v)
	}
}
