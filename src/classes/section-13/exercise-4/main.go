package main

import "fmt"

func main() {
	p := struct {
		m map[string]int
		s []string
	}{
		m: map[string]int{
			"a": 1,
			"b": 2,
		},
		s: []string{
			"blue", "red", "green", "gray",
		},
	}

	fmt.Println(p)
	fmt.Println(p.m)
	fmt.Println(p.s)

	for k, v := range p.m {
		fmt.Println(k, v)
	}

	for i, v := range p.s {
		fmt.Println(i, v)
	}
}
