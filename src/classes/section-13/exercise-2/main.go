package main

import "fmt"

type person struct {
	firstName        string
	lastName         string
	favoriteIceCream []string
}

func main() {

	p1 := person{
		firstName: "Lucas",
		lastName:  "Ribeiro",
		favoriteIceCream: []string{
			"Goiaba",
			"Pequi",
			"Morango",
		},
	}

	p2 := person{
		firstName: "Nathalia",
		lastName:  "Oliveira",
		favoriteIceCream: []string{
			"Café",
			"Jurubeba",
			"Tomate",
		},
	}

	m := map[string]person{
		p1.lastName: p1,
		p2.lastName: p2,
	}

	for _, v := range m {
		fmt.Println(v.firstName)
		fmt.Println(v.lastName)
		for _, f := range v.favoriteIceCream {
			fmt.Println(f)
		}
	}
}
