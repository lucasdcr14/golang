package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func main() {

	p := person{
		first: "Lucas",
		last:  "Ribeiro",
		age:   22,
	}

	fmt.Println(p)
	changeMe(&p)
	fmt.Println(p)
}

func changeMe(p *person) {
	p.first = "James"
	// (*p).first = "James"
	p.last = "Bond"
	p.age = 33
}
