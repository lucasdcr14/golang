package main

import (
	"fmt"
	"runtime"
)

var a int
var b float64
var c int8 = -128

// var d int8 = -129
var d int8 = 127

// var d int8 = 128

func main() {
	x := 42
	y := 42.34534
	fmt.Println(x)
	fmt.Println(y)
	fmt.Printf("%T\n", x)
	fmt.Printf("%T\n", y)

	fmt.Println("-----")

	a = 42
	b = 42.34534
	fmt.Println(a)
	fmt.Println(b)
	fmt.Printf("%T\n", a)
	fmt.Printf("%T\n", b)

	/* fmt.Println("-----")
	a = 42.34534
	b = 42.34534
	fmt.Println(a)
	fmt.Println(b)
	fmt.Printf("%T\n", a)
	fmt.Printf("%T\n", b) */

	fmt.Println(c)
	fmt.Printf("%T", c)

	fmt.Println(d)
	fmt.Printf("%T", d)

	fmt.Println("-----")
	fmt.Println(runtime.GOOS)
	fmt.Println(runtime.GOARCH)
}
