package main

import (
	"fmt"
)

const (
	_      = iota
	kbIota = 1 << (iota * 10)
	mgIota = 1 << (iota * 10)
	gbIota = 1 << (iota * 10)
)

func main() {
	x := 2
	fmt.Printf("%d\t\t%b\n", x, x)

	y := x << 1
	fmt.Printf("%d\t\t%b\n", y, y)

	kb := 1024
	mb := 1024 * kb
	gb := 1024 * mb

	fmt.Printf("%d\t\t\t%b\n", kb, kb)
	fmt.Printf("%d\t\t\t%b\n", mb, mb)
	fmt.Printf("%d\t\t%b\n", gb, gb)

	fmt.Printf("%d\t\t\t%b\n", kbIota, kbIota)
	fmt.Printf("%d\t\t\t%b\n", mgIota, mgIota)
	fmt.Printf("%d\t\t%b\n", gbIota, gbIota)
}
