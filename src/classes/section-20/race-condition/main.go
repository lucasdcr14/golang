package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("GOroutines:", runtime.NumGoroutine())

	counter := 0

	const gs = 100

	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			v := counter
			//time.Sleep(time.Second * 5)
			runtime.Gosched()
			v++
			counter = v
			fmt.Println("GOroutines:", runtime.NumGoroutine())
			wg.Done()
		}()
		fmt.Println("PITCHIU")
	}
	wg.Wait()
	fmt.Println("GOroutines:", runtime.NumGoroutine())
	fmt.Println("count: ", counter)
}
