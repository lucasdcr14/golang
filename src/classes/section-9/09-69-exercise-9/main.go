package main

import "fmt"

func main() {

	favSport := "soccer"
	switch favSport {
	case "basketball":
		fmt.Println("Should not print")
	case "soccer":
		fmt.Println("Should print")
	case "skiing":
		fmt.Println("Should not print")
	}
}
