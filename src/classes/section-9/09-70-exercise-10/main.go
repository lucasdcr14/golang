package main

import "fmt"

func main() {

	switch {
	case true && true:
		fmt.Println("true && true")
		fallthrough
	case true && false:
		fmt.Println("true && false")
		fallthrough
	case true || true:
		fmt.Println("true || true")
		fallthrough
	case true || false:
		fmt.Println("true || false")
		fallthrough
	case !true:
		fmt.Println("!true")
	case !false:
		fmt.Println("!false")
	}
}
