package main

import "fmt"

func main() {

	x := 42

	if x == 42 {
		fmt.Println("it's 42")
	} else if x > 42 {
		fmt.Println("It's bigger than 42")
	} else {
		fmt.Println("It's fewer than 42")
	}
}
