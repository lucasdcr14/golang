// dog package allows us to more fully understand dogs.
package dog

// Years converts humans years to dog years.
func Years(age int) int {
	return age * 7
}
