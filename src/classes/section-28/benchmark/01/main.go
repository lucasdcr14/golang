package main

import (
	"classes/section-28/benchmark/01/saying"
	"fmt"
)

func main() {
	fmt.Println(saying.Greet("James"))
}
