package main

import "fmt"

var x int
var g func() = func() {
	fmt.Println("g from outside")
}

func main() {

	f := func() {
		fmt.Println("Hello from de variable f")
	}

	f()
	fmt.Printf("%T\n", f)

	fmt.Println(x)
	fmt.Printf("%T\n", x)

	g()
	g = f
	g()
	fmt.Printf("%T\n", g)

	fmt.Println("done")
}
