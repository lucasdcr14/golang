package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func (p person) speak() {
	fmt.Println("I am ", p.first, ". And I am ", p.age, "years old.")
}

func main() {
	p := person{
		"Lucas",
		"Ribeiro",
		22,
	}

	p.speak()
}
