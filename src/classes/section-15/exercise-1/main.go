package main

import "fmt"

var hotdog int

type person struct {
}

type human interface {
	speak()
}

func main() {
	fmt.Println(foo())
	fmt.Println(bar())
}

func foo() int {
	return 42
}

func bar() (int, string) {
	return 1984, "Big Brother is watching"
}
