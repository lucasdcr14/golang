package main

import (
	"fmt"
)

func main() {
	x := callFunc()

	fmt.Println(x())
}

func callFunc() func() string {
	return func() string {
		return "Hello world!"
	}
}
