package main

import "fmt"

func main() {
	/* defer foo()
	bar() */

	defer foo()
	fmt.Println("Hello, playground")
}

/* func foo() {
	fmt.Println("foo")
}

func bar() {
	fmt.Println("bar")
} */

func foo() {
	defer func() {
		fmt.Println("foo DEFER ran")
	}()

	fmt.Println("foo ran")
}
