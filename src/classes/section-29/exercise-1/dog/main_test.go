package dog

import (
	"fmt"
	"testing"
)

func TestYears(t *testing.T) {
	s := Years(10)
	if s != 70 {
		t.Error("got", s, "expected", 70)
	}
}

func TestYearsTwo(t *testing.T) {
	s := YearsTwo(10)
	if s != 70 {
		t.Error("got", s, "expected", 70)
	}
}

func exampleYears() {
	fmt.Println(Years(10))
	// Output:
	// 70
}

func exampleYearsTwo() {
	fmt.Println(YearsTwo(10))
	// Output:
	// 70
}

func BenchmarkYears(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Years(10)
	}
}

func BenchmarkYearsTwo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		YearsTwo(10)
	}
}
