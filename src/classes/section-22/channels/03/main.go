package main

import "fmt"

// successful buffer
func main() {
	c := make(chan int, 1)

	c <- 42

	fmt.Println(<-c)
}
