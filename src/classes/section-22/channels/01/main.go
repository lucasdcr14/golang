package main

import "fmt"

// does not run
func main() {
	c := make(chan int)

	c <- 42

	fmt.Println(<-c)
}
