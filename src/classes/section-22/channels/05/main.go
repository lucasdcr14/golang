package main

import "fmt"

// unsuccessful buffer
func main() {
	c := make(chan int, 2)
	s := make(chan string, 1)

	c <- 42
	c <- 43

	fmt.Println(<-c)
	fmt.Println(<-c)

	s <- "Hello, World"

	fmt.Println(<-s)
}
