package main

import (
	"fmt"
	"runtime"
)

func main() {

	c := make(chan int)

	// send
	go foo(c)

	// receive
	bar(c)

	fmt.Println("about to exit")
}

// send
func foo(c chan<- int) {
	c <- 42
	fmt.Println(runtime.NumGoroutine())
}

// receive
func bar(c <-chan int) {
	fmt.Println(<-c)
}
