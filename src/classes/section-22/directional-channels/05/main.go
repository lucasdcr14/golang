package main

import "fmt"

func main() {
	c := make(chan int, 2)
	cr := make(chan<- int) // receive
	cs := make(<-chan int) //send

	fmt.Println("-----")
	fmt.Println("c\t%T\n", c)
	fmt.Println("cr\t%T\n", cr)
	fmt.Println("cs\t%T\n", cs)
}
