package main

import "fmt"

func main() {
	c := make(chan int)
	cr := make(chan<- int) // receive
	cs := make(<-chan int) //send

	fmt.Println("-----")
	fmt.Println("c\t%T\n", c)
	fmt.Println("cr\t%T\n", cr)
	fmt.Println("cs\t%T\n", cs)

	// specific to general doesn't assign
	c = cr
	c = cs
}
