package main

import "fmt"

// does not work
func main() {
	c := make(chan<- int, 2)

	c <- 42
	c <- 43

	fmt.Printf("%T\n", c)
	fmt.Println(<-c)
	fmt.Println(<-c)
	fmt.Println("-----")
}
