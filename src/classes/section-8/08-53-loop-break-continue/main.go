package main

import (
	"fmt"
)

func main() {
	x := 83 / 40
	y := 83 % 40
	fmt.Println(x, y)

	i := 0
	for {
		i++
		if i > 100 {
			break
		}
		if i%2 != 0 {
			continue
		}

		fmt.Println(i)
	}
}
