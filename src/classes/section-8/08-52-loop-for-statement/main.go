package main

import (
	"fmt"
)

func main() {

	// for statemnets with single condition
	x := 1
	for x < 10 {
		fmt.Println(x)
		x++
	}
	fmt.Println("Done.")

	// for statements with for clause
	for y := 0; y < 10; y++ {
		fmt.Println(y)
	}
	fmt.Println("Done.")

	x = 0
	for {
		if x > 9 {
			break
		}
		fmt.Println(x)
		x++
	}
	fmt.Println("done.")
}
