package main

import "fmt"

func main() {
	x := []int{4, 5, 7, 8, 42}
	fmt.Println(x[1])
	fmt.Println(x)
	fmt.Println(x[:])
	fmt.Println(x[1:])
	fmt.Println(x[0:3])

	for i := 0; i < len(x); i++ {
		fmt.Println(i, x[i])
	}

	for i, v := range x {
		fmt.Println(i, v)
	}
}
